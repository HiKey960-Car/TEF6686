#ifndef TEF6686_H
#define TEF6686_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "Tuner_Api.h"
#include "Tuner_Proc.h"
#include "Tuner_Api_Lithio.h"
#include "Tuner_Drv_Lithio.h"
#include "Tuner_Interface.h"

#define I2C_ADDR 0x64

#endif

